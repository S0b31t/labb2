//
//  storyData.m
//  sagoApp
//
//  Created by Johan H on 2015-02-01.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import "storyData.h"


@interface storyData()

@property (nonatomic) NSArray *textFirst;
@property (nonatomic) NSArray *textSecond;
@property (nonatomic) NSArray *textThird;

@end

@implementation storyData

-(NSString*)randomElement:(NSArray*)array {
    return array[arc4random() % array.count];
}

-(NSString*)simpleStory {
    NSString *randomTextFirst=[self randomElement:self.textFirst];
    NSString *randomTextSecond=[self randomElement:self.textSecond];
    NSString *randomTextThird=[self randomElement:self.textThird];
    NSString *randomTextFirst2=[self randomElement:self.textFirst];
    NSString *randomTextSecond2=[self randomElement:self.textSecond];
    NSString *randomTextThird2=[self randomElement:self.textThird];
    NSString *randomTextFirst3=[self randomElement:self.textFirst];
    NSString *randomTextSecond3=[self randomElement:self.textSecond];
    NSString *randomTextThird3=[self randomElement:self.textThird];
    NSString *randomTextFirst4=[self randomElement:self.textFirst];
    
    NSString *fullStory = [NSString stringWithFormat:@"Det började med %@ som fångade %@ den var %@ men sedan %@ och efter allt detta %@ det kom en springande man %@. Detta gjorde att %@ vilket var mycket %@. Det hela slutade med %@ för att sedan så kom %@.", randomTextFirst, randomTextSecond, randomTextThird, randomTextFirst2, randomTextSecond2, randomTextThird2, randomTextFirst3, randomTextSecond3, randomTextThird3, randomTextFirst4];
    return fullStory;
}

-(NSString*)romanceStory {
    NSString *randomTextFirst=[self randomElement:self.textFirst];
    NSString *randomTextSecond=[self randomElement:self.textFirst];
    NSString *randomTextThird=[self randomElement:self.textFirst];
    NSString *randomTextFirst2=[self randomElement:self.textFirst];
    NSString *randomTextSecond2=[self randomElement:self.textSecond];
    NSString *randomTextThird2=[self randomElement:self.textThird];
    NSString *randomTextFirst3=[self randomElement:self.textFirst];
    NSString *randomTextSecond3=[self randomElement:self.textSecond];
    NSString *randomTextThird3=[self randomElement:self.textThird];
    NSString *randomTextFirst4=[self randomElement:self.textFirst];
    
    NSString *fullStory = [NSString stringWithFormat:@"Det började med %@ som fångade %@ den var %@ men sedan %@ och efter allt detta %@ det kom en springande man %@. Detta gjorde att %@ vilket var mycket %@. Det hela slutade med %@ för att sedan så kom %@.", randomTextFirst, randomTextSecond, randomTextThird, randomTextFirst2, randomTextSecond2, randomTextThird2, randomTextFirst3, randomTextSecond3, randomTextThird3, randomTextFirst4];
    return fullStory;
}

-(NSString*)actionStory {
    NSString *randomTextFirst=[self randomElement:self.textFirst];
    NSString *randomTextSecond=[self randomElement:self.textSecond];
    NSString *randomTextThird=[self randomElement:self.textThird];
    NSString *randomTextFirst2=[self randomElement:self.textSecond];
    NSString *randomTextSecond2=[self randomElement:self.textSecond];
    NSString *randomTextThird2=[self randomElement:self.textSecond];
    NSString *randomTextFirst3=[self randomElement:self.textFirst];
    NSString *randomTextSecond3=[self randomElement:self.textSecond];
    NSString *randomTextThird3=[self randomElement:self.textThird];
    NSString *randomTextFirst4=[self randomElement:self.textFirst];
    
    NSString *fullStory = [NSString stringWithFormat:@"Det började med %@ som fångade %@ den var %@ men sedan %@ och efter allt detta %@ det kom en springande man %@. Detta gjorde att %@ vilket var mycket %@. Det hela slutade med %@ för att sedan så kom %@.", randomTextFirst, randomTextSecond, randomTextThird, randomTextFirst2, randomTextSecond2, randomTextThird2, randomTextFirst3, randomTextSecond3, randomTextThird3, randomTextFirst4];
    return fullStory;
}

-(NSString*)horrorStory {
    NSString *randomTextFirst=[self randomElement:self.textFirst];
    NSString *randomTextSecond=[self randomElement:self.textSecond];
    NSString *randomTextThird=[self randomElement:self.textThird];
    NSString *randomTextFirst2=[self randomElement:self.textFirst];
    NSString *randomTextSecond2=[self randomElement:self.textSecond];
    NSString *randomTextThird2=[self randomElement:self.textThird];
    NSString *randomTextFirst3=[self randomElement:self.textFirst];
    NSString *randomTextSecond3=[self randomElement:self.textThird];
    NSString *randomTextThird3=[self randomElement:self.textThird];
    NSString *randomTextFirst4=[self randomElement:self.textThird];
    
    NSString *fullStory = [NSString stringWithFormat:@"Det började med %@ som fångade %@ den var %@ men sedan %@ och efter allt detta %@ det kom en springande man %@. Detta gjorde att %@ vilket var mycket %@. Det hela slutade med %@ för att sedan så kom %@.", randomTextFirst, randomTextSecond, randomTextThird, randomTextFirst2, randomTextSecond2, randomTextThird2, randomTextFirst3, randomTextSecond3, randomTextThird3, randomTextFirst4];
    return fullStory;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textFirst = @[@"en kyss", @"en kram", @"en lång blick", @"en kväll", @"en kamrat", @"ett hjärta", @"en chokladbit", @"en blomsterbucket", @"en klapp på kinden", @"en söt tjej", @"en lång promenad på stranden", @"en kylig kväll", @"mys", @"gos", @"söt", @"fin"];
    self.textSecond = @[@"en granat", @"en pistol", @"en bomb", @"ett brinnande hus", @"en gangster", @"en machete", @"en misil", @"en brinnande bil", @"ett granatgevär", @"en pikadoll", @"en bombare", @"ett helt vanligt hus"];
    self.textThird = @[@"ett spöke", @"ett monster", @"en arg mamma", @"en seriemördare", @"flera spöken", @"flera monster", @"flera arga mammor", @"flera seriemördare", @"en exorcist", @"ett blått monster", @"en arg pappa", @"en demon"];
}

@end
