//
//  ViewController.m
//  sagoApp
//
//  Created by Johan H on 2015-01-26.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import "ViewController.h"
#import "storyData.h"

@interface ViewController ()


@property (weak, nonatomic) IBOutlet UITextView *StoryBoard;
@property (strong, nonatomic) storyData * generator;

@end

@implementation ViewController

- (IBAction)generateStory:(id)sender {
    self.StoryBoard.text = [self.generator simpleStory];
}

- (IBAction)romanceSwitch:(id)sender {
    if(!(self.StoryBoard.text = [self.generator simpleStory]))
    {
        self.StoryBoard.text = [self.generator simpleStory];
    }
    else
    {
    self.StoryBoard.text =[self.generator romanceStory];
    }
}
- (IBAction)actionSwitch:(id)sender {
    if(!(self.StoryBoard.text = [self.generator simpleStory]))
    {
        self.StoryBoard.text = [self.generator simpleStory];
    }
    else
    {
        self.StoryBoard.text =[self.generator actionStory];
    }
}
- (IBAction)horrorSwitch:(id)sender {
    if(!(self.StoryBoard.text = [self.generator simpleStory]))
    {
        self.StoryBoard.text = [self.generator simpleStory];
    }
    else
    {
        self.StoryBoard.text =[self.generator horrorStory];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.generator = [[storyData alloc] init];
    [self.generator viewDidLoad];
    }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
