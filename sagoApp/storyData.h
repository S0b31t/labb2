//
//  storyData.h
//  sagoApp
//
//  Created by Johan H on 2015-02-01.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface storyData : UIViewController

-(NSString*)randomElement:(NSArray*)array;
-(NSString*)simpleStory;
-(NSString*)romanceStory;
-(NSString*)actionStory;
-(NSString*)horrorStory;

@end
