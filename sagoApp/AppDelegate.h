//
//  AppDelegate.h
//  sagoApp
//
//  Created by Johan H on 2015-01-26.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

